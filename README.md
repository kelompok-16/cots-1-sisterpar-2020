# Jadwal Konser Musik

## Deskripsi Program
Program ini dibuat untuk membantu user melihat jadwal konser dari dua agency besar korea,
yaitu SMTOWN dan YG Entertainment. Program ini dibuat menggunakan Inderect Communication 
yang terdapat publisher, subscriber, dan broker. Pertama user ingin mengetahui jadwal konser,
kemudian publisher akan menampilkan pilihan jadwal konser. Setelah itu user akan memilih konser mana
yang ingin dilihat jadwalnnya, dan terakhir publisher akan menampilkan info jadwal sesuai konser yang
dipilih oleh user.

## Pembagian Tugas

# Nisa Aulia
- membuat program dari publisher("pub.py")
- menguji program dan mencari kesalahan dari suatu program 
- membuat sebagian dokumen README
- membuat dokumentasi program

# Abi Rafdhi Hernandy
- membuat seluruh program dari subscriber("sub.py")
- menguji program dan maintenance 
- membuat sebagian dokumen readme



# Instalasi Program
1. Buka terminal ubuntu kemudian masuk root
2. Lalu masuk ke directory yang isinya terdapat file pub.py dan sub.py
3. Setelah itu, jalankan broker dahulu dengan syntax mosquitto -m (nomor port)
4. Kemudian, jalankan file sub.py dengan syntax python3 sub.py
5. Setelah itu terdapat pilihan jadwal yang dapat dipilih oleh user
5. Lalu, buka terminal baru masuk ke root dan masuk ke diretory penyimpanan file pub.py
6. Kemudian jalankan file pub.py dengan syntax python3 pub.py
7. Dan publisher mengirimkan info mengenai jadwal yang ingin dipilih user
8. Dan program siap dijalankan, dengan tata cara lewat gambar akan dijelaskan lebih lanjut lewat wiki