# import paho mqtt
import paho.mqtt.client as mqtt

# import time untuk sleep()
import time

# import datetime untuk mendapatkan waktu dan tanggal
from datetime import datetime

# definisikan nama broker yang akan digunakan
broker_address = "localhost"

# buat client baru bernama P2
client = mqtt.Client("P2")

# koneksi ke broker
print("connecting to broker")
client.connect(broker_address, port=1883)

# mulai loop client
client.loop_start()

# penanda akan mempublish sesuatu
print("publish something")
# sleep 1 detik
time.sleep(1)
# publish jadwal konser 1
client.publish("konser1","          KPOP CONCERT              \n-------------SMTOWN-------------\n         자랑스럽게 선물            \n-------------------------------\n          KOREAN BALLAD           \n-------------------------------\nINDONESIA CONVENTION EXHIBITION HALL  \n        Saturday 12 Oct 2020        \n           7:00 PM           \n\n")
# publish jadwal konser 2
client.publish("konser2","          KPOP CONCERT              \n---------YG ENTERTAINMENT-------\n         자랑스럽게 선물            \n-------------------------------\n            GIRLBAND           \n-------------------------------\nSENTUL INTERNATIONAL CONVENTION CENTER\n        Saturday 13 June 2020        \n           7:00 PM           ")		
#stop loop
client.loop_stop()
