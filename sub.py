import os

 # import paho mqtt
import paho.mqtt.client as mqtt

# import time for sleep()
import time

# import re (regular expression) untuk filter
import re

# buat callback on_message; jika ada pesan
# maka fungsi ini akan dipanggil secara asynch
########################################
def on_message(client, userdata, message):
    # filter pesan yang masuk
	print( str(message.payload.decode("utf-8")))


# buat definisi nama broker yang akan digunakan
broker_address = "localhost"

# buat client baru bernama P1
print("creating new instance")
client = mqtt.Client("P1")

# kaitkan callback on_message ke client
client.on_message = on_message

# buat koneksi ke broker
print("connecting to broker")
client.connect(broker_address,port=1883)

# jalankan loop client
client.loop_start()

# client melakukan subsribe ke topik 1 dan topik 2
print("1.SMTOWN SCHEDULE") #menampilkan pilihan topik 1
print("2.YG ENTERTAINMET SCHEDULE") # menampilkan pilihan topik 2
print("3.SMTOWN AND YG ENTERTAINMET SCHEDULE") # menampilkan pilihan topik 1 dan topik 2
print("4.EXIT") # menampilkan pilihan exit
x = input("Choose Schedule : ")
if (x=="1"): # kondisi jika memilih 1
	client.subscribe("konser1") # menampilkan topik 1
elif (x=="2"): # kondisi jika memilih 2
	client.subscribe("konser2") # menampilkan topik 2
elif (x=="3"): # kondisi jika memilih 3
	client.subscribe("konser1") # menampilkan topik 1
	client.subscribe("konser2") # menampilkan topik 2
elif (x=="4"): # kondisi jika memilih 4
	y = False
else:
	print("Concert is not available")
#membersihkan screen
os.system("clear")
# loop forever
while True:
    # berikan waktu tunggu 1 detik
    time.sleep(1)

#stop loop
client.loop_stop()
